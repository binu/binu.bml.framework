<?php

return array(
    'font_size' => 60,
    'line_height' => 50,
    'title_font_size' => 60,
    'title_line_height' => 90,
    'toolbar_height' => 100,
    'indent' => 40,
);
