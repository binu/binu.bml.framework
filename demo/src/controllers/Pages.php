<?php

require_once 'Controller.php';
require_once '../src/StandaloneBmlGen.php';
require_once '../src/BmlDocument.php';

class Pages extends Controller
{
    public function __construct()
    {
        $this->bml_doc = new BmlDocument(
            TTL, DEV_ID, APP_ID, $_COOKIE, LTYPE, FOOTER_BG_COLOR,
            FOOTER_TEXT_COLOR
        );

        // set styles common to practically all pages
        $this->set_common_styles();
        $this->gen_menu();
    }

    public function home()
    {
        $bml = $this->bml_doc->get_bml();
        $this->gen_header($bml->page, 'Bml Demo');
        $psegment = $bml->page->gen_psegment();

        $psegment->gen_text(
            array(
                'text' => 'Here is some basic text, <t>and a translated part.</t>',
                'style' => 'body',
            )
        );

        $psegment->gen_text(array('text' => 'And a button:', 'style' => 'body'));

        $psegment->gen_button(
            array(
                'bg_style' => 'button_bg',
                'text' => 'Button',
                'text_style' => 'button_text',
                'url' => $this->gen_in_app_url(
                    'general_text', null, 'GeneralTextPages'
                ),
            )
        );

        $psegment->gen_link(
            array(
                'text' => 'Click here to see a form.',
                'text_style' => 'link',
                'url' => $this->gen_in_app_url('form', null, 'Forms'),
            )
        );

        $this->render_bml($bml);
    }

    public function demo_list()
    {
        $bml = $this->bml_doc->get_bml();
        $this->gen_header($bml->page, 'Demo List');

        $list_psegment = $bml->page->gen_list(
            array(
                'text_style' => 'body',
                'items' => array(
                    array('text' => 'Text: ' . $_GET['1']),
                    array('text' => 'Number: ' . $_GET['2']),
                ),
            )
        );

        $icon_w_h = round($this->bml_doc->screen_w / 4);
        $resized_url = $this->get_app_cons()->img_resize(
            $this->base_url() . 'img/home.png',
            $icon_w_h,
            $icon_w_h
        );
        $list_psegment->gen_icon_link(
            array(
                'icon_url' => $resized_url,
                'url' => $this->gen_in_app_url('home'),
                'x' => $this->bml_doc->indent,
                'w' => $icon_w_h,
                'h' => $icon_w_h,
            )
        );

        $this->render_bml($bml);
    }
}
