<?php

require_once 'Controller.php';
require_once '../src/GeneralTextBmlDoc.php';

class GeneralTextPages extends Controller
{
    public function general_text()
    {
        $text = 'This is an example of a subclass of BmlDocument that generates a '
                . 'very basic page like this with simple text.';
        $this_ref = $this;
        $this->bml_doc = new GeneralTextBmlDoc(
            TTL, DEV_ID, APP_ID, $_COOKIE, $text, 'body', LTYPE, FOOTER_BG_COLOR,
            FOOTER_TEXT_COLOR, null,
            function (BmlElement $bml_page) use ($this_ref) {
                $this_ref->gen_header($bml_page, 'Demo General Text');
            }
        );

        $this->set_common_styles();
        $this->gen_menu();
        $this->render_bml($this->bml_doc->get_bml());
    }
}
